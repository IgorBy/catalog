<?php

require_once "../DB.php";
$table = require_once '../configTable.php';
$tableCategories = $table['tableCategories'];

$title = $parent = "";
$title_err = $title_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $input_title = trim($_POST["title"]);
    if(empty($input_title)){
        $title_err = "Please enter a name.";
    } elseif(!filter_var($input_title, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $title_err = "Please enter a valid name.";
    } else{
        $title = $input_title;
    }
    
    $input_parent = trim($_POST["parent"]);
    if(empty($input_parent)){
        $parent_err = "Please enter the salary amount.";     
    } elseif(!ctype_digit($input_parent)){
        $parent_err = "Please enter a positive integer value.";
    } else{
        $parent = $input_parent;
    }

    if(empty($title_err) && empty($parent_err)){

        $db = new DB();
        $db->connect();

        $sql = "INSERT INTO $tableCategories SET `title` = :title,`parent` = :parent";

        $params =  [
            'title' => $title,
            'parent' => $parent
        ];

        if ($db->exec($sql, $params)) {
            header("location: crud-categories.php");
        } else {
            echo "Something went wrong. Please try again later.";
        }
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Create Record</h2>
                    </div>
                    <p>Please fill this form and submit to add categories record to the database.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($title_err)) ? 'has-error' : ''; ?>">
                            <label>title</label>
                            <textarea name="title" class="form-control"><?php echo $title; ?></textarea>
                            <span class="help-block"><?php echo $title_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($parent_err)) ? 'has-error' : ''; ?>">
                            <label>parent category</label>
                            <input type="text" name="parent" class="form-control" value="<?php echo $parent; ?>">
                            <span class="help-block"><?php echo $parent_err;?></span>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="../crud-categories/crud-categories.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>