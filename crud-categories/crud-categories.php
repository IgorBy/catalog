<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style type="text/css">
        .wrapper{
            width: 650px;
            margin: 0 auto;
        }
        .page-header h2{
            margin-top: 0;
        }
        table tr td:last-child a{
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Categories Details</h2>
                        <a href="create.php" class="btn btn-success pull-right">Add New Categories</a>
                    </div>
                    <?php

                    require_once '../DB.php';
                    $table = require_once '../configTable.php';
                    $tableCategories = $table['tableCategories'];

                    $db = new DB();
                    $db->connect();

                    $sql = "SELECT * FROM $tableCategories";
                    if($result = $db->query($sql, null)){
                        echo "<table class='table table-bordered table-striped'>";
                        echo "<thead>";
                        echo "<tr>";
                        echo "<th>#</th>";
                        echo "<th>title</th>";
                        echo "<th>parent category</th>";
                        echo "</tr>";
                        echo "</thead>";
                        echo "<tbody>";
                        foreach ($result as $item) {

                            echo "<tr>";
                            echo "<td>" . $item['id'] . "</td>";
                            echo "<td>" . $item['title'] . "</td>";
                            echo "<td>" . $item['parent'] . "</td>";
                            echo "<td>";
                            echo "<a href='read.php?id=". $item['id'] ."' title='View Record' data-toggle='tooltip'><span class='glyphicon glyphicon-eye-open'></span></a>";
                            echo "<a href='update.php?id=". $item['id'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                            echo "<a href='delete.php?id=". $item['id'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                            echo "</td>";
                            echo "</tr>";

                        }
                        echo "</tbody>";
                        echo "</table>";

                    } else{
                        echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
                    }

                    ?>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>