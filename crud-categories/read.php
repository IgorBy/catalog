<?php

if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){

    $table = require_once '../configTable.php';
    $tableCategories = $table['tableCategories'];

    $param_id = trim($_GET["id"]);

    require_once "../DB.php";

    $db = new DB();
    $db->connect();

    $sql  = "SELECT * FROM $tableCategories WHERE `id` = :id";

    $params =  [
        'id' => $param_id,
    ];

    $query = $db->query($sql,$params);

    if ($query) {
        foreach ($query as $row) {
            $id = $row['id'];
            $title = $row['title'];
            $parent = $row['parent'];
        }
    }else {
        echo "Oops! Something went wrong. Please try again later.";
    }

} else{
    header("location: error.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1>View Record</h1>
                    </div>
                    <div class="form-group">
                        <label>id</label>
                        <p class="form-control-static"><?php echo $row["id"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label>title</label>
                        <p class="form-control-static"><?php echo $row["title"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label>parent</label>
                        <p class="form-control-static"><?php echo $row["parent"]; ?></p>
                    </div>
                    <p><a href="/crud-categories/crud-categories.php" class="btn btn-primary">Back</a></p>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>