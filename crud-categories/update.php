<?php

require_once "../DB.php";
$table = require_once '../configTable.php';
$tableCategories = $table['tableCategories'];

$title = $parent = "";
$title_err = $title_err = "";

if(isset($_POST["id"]) && !empty($_POST["id"])){
    
    $id = $_POST["id"];

    $input_title = trim($_POST["title"]);
    if(empty($input_title)){
        $title_err = "Please enter a title.";
    } elseif(!filter_var($input_title, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $title_err = "Please enter a valid title.";
    } else{
        $title = $input_title;
    }

    $input_parent = trim($_POST["parent"]);
    if(empty($input_parent)){
        $parent_err = "Please enter the parent amount.";
    } elseif(!ctype_digit($input_parent)){
        $parent_err = "Please enter a positive integer value.";
    } else{
        $parent = $input_parent;
    }

    if(empty($title_err) && empty($address_err) && empty($parent_err)){

        $db = new DB();
        $db->connect();

        $sql = "UPDATE $tableCategories SET `title` = :title,`parent` = :parent WHERE `id` = :id";

        $params =  [
            'title' => $title,
            'parent' => $parent,
            'id' => $id,
        ];

        if ($db->exec($sql, $params)) {
            header("location: crud-categories.php");
        } else {
            echo "Something went wrong. Please try again later.";
        }
    }

} else{

    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){

        $id =  trim($_GET["id"]);

        $db = new DB();
        $db->connect();

        $sql  = "SELECT * FROM $tableCategories WHERE `id` = :id";

        $params =  [
            'id' => $param_id,
        ];

        $query = $db->query($sql,$params);

        if ($query) {
            foreach ($query as $row) {
                $id = $row['id'];
                $title = $row['title'];
                $parent = $row['parent'];
            }
        }
    }  else{
        header("location: error.php");
        exit();
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Record</h2>
                    </div>
                    <p>Please edit the input values and submit to update the record.</p>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                        <div class="form-group <?php echo (!empty($title_err)) ? 'has-error' : ''; ?>">
                            <label>title</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $title; ?>">
                            <span class="help-block"><?php echo $title_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($parent_err)) ? 'has-error' : ''; ?>">
                            <label>parent</label>
                            <input type="text" name="parent" class="form-control" value="<?php echo $parent; ?>">
                            <span class="help-block"><?php echo $parent_err;?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="/crud-categories/crud-categories.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>