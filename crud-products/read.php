<?php

if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){

    $param_id = trim($_GET["id"]);

    require_once "../DB.php";
    $table = require_once '../configTable.php';
    $tableProducts = $table['tableProducts'];

    $db = new DB();
    $db->connect();

    $sql  = "SELECT * FROM $tableProducts WHERE `id` = :id";

    $params =  [
        'id' => $param_id,
    ];

    $query = $db->query($sql,$params);

    if ($query) {
        foreach ($query as $row) {
            $id = $row['id'];
            $title = $row['title'];
            $parent = $row['parent'];
            $parent = $row['image'];
            $parent = $row['price'];
        }
    }else {
        echo "Oops! Something went wrong. Please try again later.";
    }

} else{
    header("location: error.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1>View Record</h1>
                    </div>
                    <div class="form-group">
                        <label>id</label>
                        <p class="form-control-static"><?php echo $row["id"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label>title</label>
                        <p class="form-control-static"><?php echo $row["title"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label>parent</label>
                        <p class="form-control-static"><?php echo $row["parent"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label>image</label>
                        <p class="form-control-static"><?php echo $row["image"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label>price</label>
                        <p class="form-control-static"><?php echo $row["price"]; ?></p>
                    </div>
                    <p><a href="/crud-products/crud-products.php" class="btn btn-primary">Back</a></p>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>