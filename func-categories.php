<?php

function print_arr($arr) {
    echo  "<pre>" . print_r($arr, true) . "</pre>";
}

function get_arr_categories($categories) {

    foreach ($categories as $category => $category_value) {
        foreach ($category_value as $item => $value) {
            $arr_categories[$category_value['id']] = $category_value;
        }
    }

    return $arr_categories;
}

function get_categories_id($array, $id) {
    if (!$id) return false;

    foreach ($array as $item => $value) {
        foreach ($value as $item2 => $value2) {
            if ($value['parent'] == $id) {
                /** @var $data */
                $data .= $value['id']. ', ';
                $data .= get_categories_id($array, $value['id']);
            }
        }
    }

    return $data;

}

function getTree($dataset) {
    $tree = array();

    foreach ($dataset as $id => &$node) {

        if (!$node['parent']){
            $tree[$id] = &$node;
        }else{

            $dataset[$node['parent']]['childs'][$id] = &$node;
        }
    }
    return $tree;
}

function categories_to_template($category){
    ob_start();
    require "category_template.php";
    return ob_get_clean();
};

function categories_to_string($data) {
    foreach ($data as $item){
        /** @var $string */
        $string .= categories_to_template($item);
    }
    return $string;
}
