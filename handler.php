<?php

require_once 'DB.php';
require_once 'func-categories.php';

$db = new DB();
$db->connect();

$table = require_once 'configTable.php';
$tableProducts = $table['tableProducts'];
$tableCategories = $table['tableCategories'];

$sql_categories = "SELECT * FROM $tableCategories";

$arr_categories = $db->query($sql_categories, null);

$new_arr_categories = get_arr_categories($arr_categories);
$categories_tree = getTree($new_arr_categories);
$categories_menu = categories_to_string($categories_tree);

if (!empty($_GET['category'])) {
    $id = (int)$_GET['category'];

    $ids = get_categories_id($new_arr_categories, $id);

    $ids = !$ids ? $id : $ids;

    if ($ids){
        $sql_products = "SELECT * FROM $tableProducts WHERE parent IN($ids) ";
    }

    $arr_products = $db->query($sql_products, null);
} else {
    $sql_products = "SELECT * FROM $tableProducts ORDER  BY 'title'";
    $arr_products = $db->query($sql_products, null);
}
