<?php

session_start();
require_once 'handler.php';

?>

<html>
<head>
    <title>Категории</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
</head>
<body>

<div class="wrapper">
    <div class="wrapper-header">
        <?php if (!empty($_SESSION['name'])) { ?>
            <div class="header-btn">
                <a href="crud-categories/crud-categories.php" type="button" class="btn btn-warning btn-icon">
                    <span class="icon"><i class="fas fa-cog fa-spin"></i></span>Setting Categories
                </a>
                <a href="crud-products/crud-products.php" type="button" class="btn btn-warning btn-icon">
                    <span class="icon"><i class="fas fa-cog fa-spin"></i></span>Setting products
                </a>
            </div>
        <?php } else { ?>
            <?php
            echo '<p> Авторизуйся чтобы редактировать</p>';
        } ?>
        <div class="header-auth">
            <?php if (empty($_SESSION['name'])) { ?>
                <a href="auth/form-auth.php" type="button" class="btn btn-info">Авторизация</a>
                <a href="auth/signup.php" type="button" class="btn btn-primary">Регистрация</a>
            <?php } else { ?>
                <?php echo '<div class="message">Добро пожаловать'.' '.  $_SESSION['name'] .' </div>';
                echo '<a class="logout" href="auth/logout.php">Выйти</a>';
            } ?>
        </div>
    </div>

    <div class="wrapper-body">
        <div class="body-sidebar">
            <ul class="category">
                <?php echo $categories_menu ?>
            </ul>
        </div>
        <div class="body-content">
            <?php if(is_array($arr_products) && !empty($arr_products)) :?>

                <?php foreach ($arr_products as $product):?>
                    <div class="product">
                        <div class="product-img">
                            <img style="max-width: 99%" src="image/<?=$product['image']?>" alt="">
                        </div>

                        <div class="product-content">
                            <div class="product-title">
                                <p style="text-align: center"><?=$product['title']?></p>
                            </div>
                            <div class="product-price">
                                <p  style="text-align: center"><?=$product['price'] . "$"?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php else: ?>
                <p>Товаров нет!</p>
            <?php endif;?>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/common.js"></script>

</body>
</html>
