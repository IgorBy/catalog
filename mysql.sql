-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 16 2018 г., 20:23
-- Версия сервера: 5.7.20
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `category-api`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent`) VALUES
(2, 'АВТОМОБИЛЬНЫЕ ТОВАРЫ', 0),
(3, 'АКСЕССУАРЫ ДЛЯ ВОЛОС', 0),
(4, 'АКСЕССУАРЫ ДЛЯ ОБУВИ', 0),
(5, 'ДОМ И ДАЧА', 0),
(6, 'Автокружки', 2),
(7, 'Брызговики', 2),
(8, 'Защита картера', 2),
(9, 'Козырьки для авто', 2),
(10, 'Банты', 3),
(11, 'Гребни', 3),
(12, 'Диадемы', 3),
(13, 'Губки для обуви', 4),
(14, 'Стельки', 4),
(15, 'Бассейны', 5),
(16, 'Бортики для кроватей', 5),
(17, 'Вазы', 5),
(18, 'Горшки для растений', 5),
(19, 'Newww', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL,
  `image` varchar(30) NOT NULL,
  `price` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `title`, `parent`, `image`, `price`) VALUES
(1, 'Автостакан', 6, 'empty.jpeg', 40),
(2, 'Автостакан 2', 6, 'empty.jpeg', 50),
(3, 'Брызговик 1', 7, 'empty.jpeg', 10),
(4, 'Брызговик 2', 7, 'empty.jpeg', 20),
(5, 'Брызговик 3', 7, 'empty.jpeg', 30),
(6, 'Брызговик 4', 7, 'empty.jpeg', 40),
(7, 'Брызговик 5', 7, 'empty.jpeg', 50),
(8, 'Защита 1', 8, 'empty.jpeg', 40),
(9, 'Защита 2', 8, 'empty.jpeg', 50),
(10, 'Защита 3', 8, 'empty.jpeg', 60);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `age`, `password`) VALUES
(1, 'igor', 24, 'rap'),
(2, 'ifg', 2332, '3r23'),
(3, 'vova', 25, '2e2d7fe5d75b602595df021c7841243b');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
